<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="root.persistence.entities.Diccionario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Diccionario> dicc = (List<Diccionario>) request.getAttribute("lista");
    Iterator<Diccionario> iterDicc = dicc.iterator();
%>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado</title>
    </head>
    <body>
        <div class="container">
            <h2>Resultado Diccionario Oxford</h2>
            <div class="form-group">
                <label for="rut" class="control-label col-sm-2">Palabra Consultada :</label>
                <div class="col-sm-15">
                    <input name="palabra" value="<%= request.getAttribute("palabra")%>" disabled class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="nombre" class="control-label col-sm-2">Significado :</label>
                <div class="col-sm-15">
                    <textarea name="significado" rows="2" cols="50" disabled class="form-control">
                        <%= request.getAttribute("significado").toString().trim()%>
                    </textarea>
                </div>
            </div> 
        </div>
        <br>
        <div class="container" align="center">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <input class="btn btn-primary" onclick="history.back()" type="button" value="Volver"/>
                </div>
            </div>
        </div>
        <div class="container">
            <h2>Historico de Consultas</h2>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Palabra</th>    
                        <th>Significado</th>
                        <th>Estado</th>            
                    </tr>
                </thead>
                <tbody>                    
                    <%
                        while (iterDicc.hasNext()) {
                            Diccionario diccionario = iterDicc.next();
                            if (diccionario.getEstado()) {
                    %><tr class="success"><%
                                } else {
                        %><tr class="danger"><%}
                        %>
                        <td><%= diccionario.getPalabra()%></td>
                        <td><%= diccionario.getSignificado()%></td>
                        <td>
                            <%
                                if (diccionario.getEstado()) {
                            %><label>Encontrado</label><%} else {
                            %><label>Sin resultado</label><%
                                }%>
                        </td>
                    </tr>
                    <%
                        }
                    %>                    
                </tbody>
            </table>
        </div>
                <footer class="fixed-bottom">
            <div class="container-fluid text-center bg-dark text-white">
                <span>Taller APP Empresariales - EVALUACION FINAL - Francisca Soto Fernandez</span>
            </div>
        </footer>
    </body>
</html>
